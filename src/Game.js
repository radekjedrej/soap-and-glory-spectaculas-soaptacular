import BootState from './phaser/states/Boot';
import PreloadState from './phaser/states/Preload';
import MenuState from './phaser/states/Menu';
import HowToPlayState from './phaser/states/HowToPlay';
import PrizesState from './phaser/states/Prizes';
import TightRopeState from './phaser/states/TightRope';
import TightRopeLevel2State from './phaser/states/TightRopeLevel2';
import LoseState from './phaser/states/Lose';
import GameOneWonState from './phaser/states/GameOneWon';
import PrizeWonState from './phaser/states/PrizeWon';
import PrizeLoseState from './phaser/states/PrizeLose';
import DataCaptureState from './phaser/states/DataCapture';
import WelcomeBackState from './phaser/states/WelcomeBack';

import './styles/styles.scss';

class Game extends Phaser.Game {
    constructor(location = null) {
        const gameWrapper = document.getElementById(location);
        const width = 640;
        const height = 700;

        super(width, height, Phaser.CANVAS, gameWrapper, null);

        this.state.add('Boot', BootState);
        this.state.add('Preload', PreloadState);
        this.state.add('Menu', MenuState);
        this.state.add('HowToPlay', HowToPlayState);
        this.state.add('Prizes', PrizesState);
        this.state.add('TightRope', TightRopeState);
        this.state.add('TightRopeLevel2', TightRopeLevel2State);
        this.state.add('Lose', LoseState);
        this.state.add('GameOneWon', GameOneWonState);
        this.state.add('PrizeWon', PrizeWonState);
        this.state.add('PrizeLose', PrizeLoseState);
        this.state.add('DataCapture', DataCaptureState);
        this.state.add('WelcomeBack', WelcomeBackState);

        this.state.start('Boot');
    }
}

window.onload = () => {
    const MyGame = new Game('game-wrapper');

    window.readyGameGlobal = {
        game: MyGame,
        wrapper: document.querySelector('.js-wrapper'),
        stateFadeTime: 200,
        stateFadeColor: '0xae1065',
        canPlaySounds: true,
        gameSounds: {},
        // playButtonSound: function () {
        //     if (window.readyGameGlobal.canPlaySounds) {
        //         window.readyGameGlobal.gameSounds.buttonClick.play();
        //     }
        // },
        stateFade: function(inOrOut = null, state = null, ...addition) {
            //const inOrOut = inOrOut;
            //var state = state;
            //var addition = addition;

            if (inOrOut !== null) {
                if (inOrOut === 'in') {
                    // Fade in

                    window.readyGameGlobal.game.camera.fade(
                        window.readyGameGlobal.stateFadeColor,
                        0
                    );
                    window.readyGameGlobal.game.camera.flash(
                        window.readyGameGlobal.stateFadeColor,
                        window.readyGameGlobal.stateFadeTime
                    );
                } else {
                    // Fade out

                    window.readyGameGlobal.game.camera.fade(
                        window.readyGameGlobal.stateFadeColor,
                        window.readyGameGlobal.stateFadeTime
                    );
                    window.readyGameGlobal.game.camera.onFadeComplete.add(() =>
                        window.readyGameGlobal.game.state.start(state, true, false, addition)
                    );
                }
            } else {
                // Boot by default
                window.readyGameGlobal.game.state.start('Boot');
            }
        },
        // sendClickEvent: function (eventCategory, name, value) {
        //     const nameOfCategory = eventCategory; // bmjCareersButton or totalPlays
        //     const nameOfEvent = name;
        //     const v = value;

        //     gtag('event', 'click', {
        //         event_category: nameOfCategory,
        //         event_label: nameOfEvent,
        //         value: v
        //     });
        // },
        addNodeListForEachPollyfill: function() {
            if (window.NodeList && !NodeList.prototype.forEach) {
                NodeList.prototype.forEach = function(callback, thisArg) {
                    thisArg = thisArg || window;
                    for (var i = 0; i < this.length; i++) {
                        callback.call(thisArg, this[i], i, this);
                    }
                };
            }
        },

        hideTCLink: () => {
            const termsLink = document.querySelector('.js-terms-link');
            termsLink.classList.add('hide');
        }
    };

    MyGame.state.onStateChange.add((newState, previousState) => {
        console.log(`${newState} state has loaded`);
    });
};
