<?php
    //header("Access-Control-Allow-Origin: *");
    require_once 'init.php';

    // Axios by default serializes JavaScript objects to JSON so we need to do some work before getting our values
    $content = file_get_contents('php://input');
    $decoded = json_decode($content, true);

    $valid_fields = false;
    $errors = [];
    $last_id = 0;

    $name     = (!empty($decoded['name'])) ? $decoded['name'] : null;
    $surname  = (!empty($decoded['surname'])) ? $decoded['surname'] : null;
    $email    = (!empty($decoded['email'])) ? $decoded['email'] : null;
    $day      = (!empty($decoded['day'])) ? $decoded['day'] : null;
    $month    = (!empty($decoded['month'])) ? $decoded['month'] : null;
    $year     = (!empty($decoded['year'])) ? $decoded['year'] : null;
    $address1 = (!empty($decoded['address1'])) ? $decoded['address1'] : null;
    $city     = (!empty($decoded['city'])) ? $decoded['city'] : null;
    $postcode = (!empty($decoded['postcode'])) ? $decoded['postcode'] : null;
    $country  = (!empty($decoded['country'])) ? $decoded['country'] : null;
    $tandc    = (!empty($decoded['tandc'])) ? $decoded['tandc'] : null;
    $signup   = (!empty($decoded['signup'])) ? $decoded['signup'] : null;
    $signup_circus   = (!empty($decoded['signup_circus'])) ? $decoded['signup_circus'] : null;
    
    $tandc  = ($tandc === true ? 1 : 0);
    $signup = ($signup === true ? 1 : 0);
    $signup_circus = ($signup_circus === true ? 1 : 0);
    
    if($day === null && $month === null && $year === null) {
        $errors['dob'] = 'You need to enter your date of birth';
    } else {
        // Combine the date fields into one string suitable for the strtotime function
        $dobString = $day . ' ' . $month . ' ' . $year;

        // Convert the dob to a valid SQL date format
        $dob = date('Y-m-d', strtotime($dobString));
    }

    // Error handling for missing fields
    if($name === null) {
        $errors['name'] = 'You need to add your name';
    }
    
    if($surname === null) {
        $errors['surname'] = 'You need to add your surname';
    }

    if($email === null) {
        $errors['email'] = 'You need to add your email';
    }

    if($address1 === null) {
        $errors['address1'] = 'You need to supply address1';
    }

    if($city === null) {
        $errors['city'] = 'You need to add your city';
    }

    if($postcode === null) {
        $errors['postcode'] = 'You need to add your postcode';
    }

    if($country === null) {
        $errors['country'] = 'You need to add your country';
    }

    if($name !== null && $surname !== null && $email !== null && $day !== null && $month !== null && $year !== null && $address1 !== null && $city !== null && $postcode !== null && $country !== null) {

        // Set to true for use in phaser to determine whether we need to load the data capture state again
        $valid_fields = true;

        // Check to see if we have a user
        $sql = "SELECT * FROM users WHERE email = :email";
        $query = $dbh->prepare($sql);
        $query->execute( array (
             'email' => $email,
        ));
        $user = $query->fetch(PDO::FETCH_OBJ);

        //$user = $dbh->query("SELECT * FROM users WHERE email = '{$email}'")->fetch(PDO::FETCH_OBJ);      

        if(!$user) { // New user

            $sql = "INSERT INTO users (name, surname, email, dob, address1, city, postcode, country, tandc, signup, signup_circus)
            VALUES (:name, :surname, :email, :dob, :address1, :city, :postcode, :country, :tandc, :signup, :signup_circus)";
            $query = $dbh->prepare($sql);
            $query->execute( array (

                'name'     => $name,
                'surname'  => $surname,
                'email'    => $email,
                'dob'      => $dob,
                'address1' => $address1,
                'city'     => $city,
                'postcode' => $postcode,
                'country'  => $country,
                'tandc'    => $tandc,
                'signup'   => $signup,
                'signup_circus' => $signup_circus
            ));

            $last_id = $dbh->lastInsertId();

        } else { // Existing user

            $sql = "UPDATE users SET name = :name, surname = :surname, email = :email, dob = :dob, address1 = :address1, city = :city, postcode = :postcode, country = :country, tandc = :tandc, signup = :signup, signup_circus = :signup_circus WHERE email = :email";
            $query = $dbh->prepare($sql);
            $query->execute( array (
                
                'name'     => $name,
                'surname'  => $surname,
                'email'    => $email,
                'dob'      => $dob,
                'address1' => $address1,
                'city'     => $city,
                'postcode' => $postcode,
                'country'  => $country,
                'tandc'    => $tandc,
                'signup'   => $signup,
                'signup_circus' => $signup_circus

            ));

            // We already have a user object from above so use the correct id
            $last_id = $user->user_id;

        }
        
    }

    // // Send this back as a test so we know we are receiving the right data TESTING
    $data = array (
        'valid_fields' => $valid_fields,
        'last_id'      => $last_id,
        'errors'       => $errors,
        'name'         => $name,
        'surname'      => $surname,
        'email'        => $email,
        'dob'          => $dob,
        'address1'     => $address1,
        'city'         => $city,
        'postcode'     => $postcode,
        'country'      => $country,
        'tandc'        => $tandc,
        'signup'       => $signup,
        'signup_circus' => $signup_circus
);

    // $data = array (
    //     'valid_fields' => $valid_fields,
    //     'last_id'      => $last_id,
    //     'errors'       => $errors,
    // );

    echo json_encode($data);