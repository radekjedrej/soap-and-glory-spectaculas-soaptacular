<?php
    require_once 'init.php';

    // Axios by default serializes JavaScript objects to JSON so we need to do some work before getting our values
    $content = file_get_contents('php://input');
    $decoded = json_decode($content, true);




    //
    // JAKE CHANGE THE GAME ODDS HERE AND ONLY HERE
	$game_odds = 50000;
    //
    //


    // In seconds. This is the check between last time user played. Basically trying to prevent someone targeting this php file on a loop
	$security_time = 0;

    // grab unix timestamp
	$time = time();

    // Grab user id from url
    $user_id = (!empty($decoded['user_id'])) ? $decoded['user_id'] : null;


    // Set up some default values for our variables
	$winner = false;

    // Array of blocked users
    $blocked_user_list = array (
    	//'10'
    );

	if($user_id !== null) { // we have a username

		if(blockedUsers($user_id, $blocked_user_list) === true) {

			$data = array(

				'winner'  => $winner,
				'outcome' => $outcome

			);

            echo json_encode($data);
            die();

		};

        // Grab our user from the database
        $sql = "SELECT * FROM users WHERE user_id = :user_id";
        $query = $dbh->prepare($sql);
        $query->execute( array (

            'user_id' => $user_id

        ));

        $user_exists = $query->fetchALL(PDO::FETCH_OBJ);

		// Technically the user should always exist as they are created in the test.php when they click playgame.
		// If not we can assume the user is targeting this file directly.
		if ($user_exists) {
			// check to see if logic code was triggered 30 seconds after playing. If so logic will run,
			// if not direct to lose page. Helps prevent ajax auto run requests
			if ($time > $user_exists[0]->security + $security_time && $user_exists[0]->total_prizes_won < 3) {

				// GAME LOGIC
                $random_number = mt_rand(1, $game_odds);


				if ( $random_number >= 1 && $random_number <= 300 ) { // WINNER

                    $prize_id = 1;
					$total_remaining = check_total_prizes($prize_id); // grab the remaining prizes

					if ( $total_remaining > 0 ) { // We have prizes

                        update_main_table($user_id);
                        update_prize_table($prize_id);
                        add_to_winners_table($user_exists[0]->email);

                        $winner = true;

                    } // total_remaining

                }

            } // $time > $user_exists[0]->security + 40 ends

		}// $user_exists

		// update the secutiy column with the latest timestamp
		// otherwise the timestamp is out of date and will always be greater
		$sql = "UPDATE users SET security = $time WHERE user_id = :user_id";
		$query = $dbh->prepare($sql);
		$query->execute( array (

			'user_id' => $user_id

		));


	} else { // User is null

		echo json_encode('Request killed');
		die();

	}


	// array to pass data back to game
	// NOTE this will be visible
	$data = array (

		'winner'  => $winner,

	);

	echo json_encode($data);
