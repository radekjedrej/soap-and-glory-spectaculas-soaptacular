<?php

/*-----------------------------------------------------------------------------------*/
/* Echo out array in a nice format
/*-----------------------------------------------------------------------------------*/
    
    function jp_array($array) {
        
        echo '<pre>', print_r($array), '</pre>';
    
    };

/*-----------------------------------------------------------------------------------*/
/* Check if user has been blocked
/*-----------------------------------------------------------------------------------*/
    
    function blockedUsers($username, $blocked_list) {

        if(in_array($username, $blocked_list)) {

            return true;

        }

        return false;

    };

/*-----------------------------------------------------------------------------------*/
/* Check total prizes remaining
/*-----------------------------------------------------------------------------------*/

    function check_total_prizes($prize_id) {
        global $dbh;
        $prize_id = $prize_id;

        $total_remaining = $dbh->query("SELECT total_remaining FROM prizes where prize_id = $prize_id")->fetchColumn(0);

        return $total_remaining;
    }

/*-----------------------------------------------------------------------------------*/
/* Update the main table
/*-----------------------------------------------------------------------------------*/

    function update_main_table($user_id) {
        global $dbh;

        // update main table
        $sql = "UPDATE users SET total_prizes_won = total_prizes_won + 1 WHERE user_id = :user_id";
        $query = $dbh->prepare($sql);
        $query->execute( array (
            
            'user_id' => $user_id
            
        ));

    }

/*-----------------------------------------------------------------------------------*/
/* Update the prizes table
/*-----------------------------------------------------------------------------------*/

    function update_prize_table($prize_id) {
        global $dbh;

        $sql = "UPDATE prizes SET total_remaining = total_remaining - 1 WHERE prize_id = $prize_id";
        $query = $dbh->prepare($sql);
        $query->execute();

    }

/*-----------------------------------------------------------------------------------*/
/* Add to our entries table
/*-----------------------------------------------------------------------------------*/

function add_to_winners_table($email) {
    global $dbh;

    $sql = "INSERT INTO winners ( winner_email ) VALUES ( :winner_email )";
    $query = $dbh->prepare($sql);
    $query->execute( array (

        'winner_email' => $email,
    
    ));

}
