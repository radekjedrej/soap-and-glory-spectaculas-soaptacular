// NOTE
//
// Seconds are in milliseconds so 1000 = 1 second
//
window.readyGameSettings = {
    level1: {
        gameTime: 30, // Game time in seconds. Will be 30 at the end but nice to be able to set it shorter for testing
        angleThatPlayerWillFall: 60, // The player will fall if they get to this angle
        angleThatPlayerCanClick: 10, // The player must be past this angle +- before they can click left or right button.
        swaySpeedMin: 1500, // We randomly generate the sway speed on left or right click. Set the min and max here. Max must be greater than min
        swaySpeedMax: 3000,
        easing: 'Cubic.easeIn' // See options below
    },
    level2: {
        gameTime: 30, // Game time in seconds. Will be 30 at the end but nice to be able to set it shorter for testing
        angleThatPlayerWillFall: 60, // The player will fall if they get to this angle
        angleThatPlayerCanClick: 10, // The player must be past this angle +- before they can click left or right button.
        swaySpeedMin: 1500, // We randomly generate the sway speed on left or right click. Set the min and max here. Max must be greater than min
        swaySpeedMax: 3000,
        easing: 'Cubic.easeIn', // See options below
        ballFrequency: 3000, // How often should we throw our balls. 1000 = 1 second
        ballSpeed: 3000 // how fast should the ball move. The lower the number, the faster the ball
    }
};

console.log('Temp game setting added');

/*

THE ONES I LIKE

Quart.easeIn
Sine.easeOut
Cubic.easeIn
*/

/*
ALL AVAILABLE EASES. REFER TO "A GUIDE TO THE TWEEN MANAGER" PHASER BOOK TO SEE HOW THEY WORK
Linear
Quad
Quad.easeOut
Quad.easeIn
Quad.easeInOut
Cubic
Cubic.easeOut
Cubic.easeIn
Cubic.easeInOut
Quart
Quart.easeOut
Quart.easeIn
Quart.easeInOut
Quint
Quint.easeOut
Quint.easeIn
Quint.easeInOut
Sine
Sine.easeOut
Sine.easeIn
Sine.easeInOut
Expo
Expo.easeOut
Expo.easeIn
Expo.easeInOut
Circ
Circ.easeOut
Circ.easeIn
Circ.easeInOut
Elastic
Elastic.easeOut
Elastic.easeIn
Elastic.easeInOut
Back
Back.easeOut
Back.easeIn
Back.easeInOut
Bounce
Bounce.easeOut
Bounce.easeIn
Bounce.easeInOut
*/
