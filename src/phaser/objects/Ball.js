export default class Ball extends Phaser.Sprite {
    constructor(game, signal = null) {
        const leftOrRight = game.rnd.integerInRange(0, 1);
        const highOrLow = game.rnd.integerInRange(0, 1);

        const x = leftOrRight === 0 ? -50 : game.width + 50;
        const y = highOrLow === 0 ? 0 : game.height;

        // Temp settings
        const settings = window.readyGameSettings.level2;

        super(game, x, y, 'gameAssets1', 'ball');

        // Constants
        this._BALL_SPEED = settings.ballSpeed || 500;

        this.game = game; // Create a class base reference
        this.anchor.setTo(0.5);
        this.scale.setTo(0.6);

        this.canSpin = true; // Can we spin the ball
        this.canBeClicked = true; // We only want to be able to click the ball once

        game.add.existing(this); // Add to the exisiting game

        // Add movement to the centre
        game.add.tween(this).to(
            {
                x: game.world.centerX,
                y: game.world.centerY - 100
            },
            this._BALL_SPEED,
            'Linear',
            true
        );

        // Scale our ball over time
        const scaleTween = game.add.tween(this.scale).to(
            {
                x: 0.3,
                y: 0.3
            },
            this._BALL_SPEED,
            'Linear',
            true
        );

        // Once complete emit our signal to say we have hit our target and destroy this ball
        scaleTween.onComplete.add(() => {
            signal.dispatch(); // Let game know we have completed our tween
            this.destroy();
            console.log('tween finished');
        });

        // Set up touch events
        this.inputEnabled = true;
        this.events.onInputDown.add(bubble => {
            if (this.canBeClicked) {
                scaleTween.stop();
                this.canBeClicked = false;
                this.destroy();
            }
        });

        // Change the cursor to a pointet on hover
        this.events.onInputOver.add(
            () => (window.readyGameGlobal.game.canvas.style.cursor = 'pointer')
        );
        this.events.onInputOut.add(
            () => (window.readyGameGlobal.game.canvas.style.cursor = 'default')
        );
    }

    update() {
        if (this.canSpin) {
            this.angle = this.angle - 2;
        }
    }
}
