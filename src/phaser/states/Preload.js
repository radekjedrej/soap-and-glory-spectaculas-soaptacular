class Preload extends Phaser.State {
    preload() {
        // State Helpers
        this.centreX = this.game.width / 2;
        this.centreY = this.game.height / 2;

        var style = {
            font: 'bold 30px Raleway',
            fill: '#ffffff',
            align: 'center'
        };

        this.add.text(this.centreX, this.centreY, 'L O A D I N G', style).anchor.setTo(0.5);

        this.load.path = './build-assets/';
        this.load.atlasJSONHash('gameAssets1', 'game-assets-1.png', 'game-assets-1.json');
        this.load.atlasJSONHash('gameAssets2', 'game-assets-2.png', 'game-assets-2.json');
    }

    create() {
        window.readyGameGlobal.stateFade('out', 'DataCapture');
    }
}

export default Preload;
