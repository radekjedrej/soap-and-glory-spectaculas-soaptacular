class Prizes extends Phaser.State {
    create() {
        // State Helpers
        this.centreX = this.game.width / 2;
        this.centreY = this.game.height / 2;

        window.readyGameGlobal.stateFade('in');

        this.add.image(0, 0, 'gameAssets1', 'prizes-bgd');

        // Add our buttons
        const backBtn = this.add.button(
            this.centreX - 150,
            this.game.height - 100,
            'gameAssets1',
            this.back,
            null,
            'back-btn-white',
            'back-btn-white'
        );
        backBtn.anchor.setTo(0.5);

        const playNow = this.add.button(
            this.centreX + 150,
            this.game.height - 100,
            'gameAssets1',
            this.playNow,
            null,
            'play-now-btn-white',
            'play-now-btn-white'
        );
        playNow.anchor.setTo(0.5);
    }

    back() {
        //Send our total plays event to google analytics
        //window.readyGameGlobal.sendClickEvent('Total number of plays', 'played', 1);
        //window.readyGameGlobal.stateFade('out', 'MainGame');
        window.readyGameGlobal.stateFade('out', 'Menu');
    }

    playNow() {
        window.readyGameGlobal.stateFade('out', 'TightRope');
    }
}

export default Prizes;
