class Lose extends Phaser.State {
    create() {
        // State Helpers
        this.centreX = this.game.width / 2;
        this.centreY = this.game.height / 2;

        window.readyGameGlobal.stateFade('in');

        this.add.image(0, 0, 'gameAssets1', 'lose-bgd');

        // Add our buttons
        const backBtn = this.add.button(
            this.centreX,
            this.game.height - 200,
            'gameAssets1',
            this.haveAnotherGo,
            null,
            'have-another-go-btn',
            'have-another-go-btn'
        );
        backBtn.anchor.setTo(0.5);

        // Add back to main menu button
        const mainMenuBtn = this.add.button(
            this.centreX,
            this.game.height - 120,
            'gameAssets1',
            this.takeMeToMainMenu,
            null,
            'main-menu-btn',
            'main-menu-btn'
        );
        mainMenuBtn.anchor.setTo(0.5);

    }

    haveAnotherGo() {
        window.readyGameGlobal.stateFade('out', 'TightRope');
    }

    takeMeToMainMenu() {
        window.readyGameGlobal.stateFade('out', 'Menu');
    }
}

export default Lose;
