class GameOneWon extends Phaser.State {
    create() {
        // State Helpers
        this.centreX = this.game.width / 2;
        this.centreY = this.game.height / 2;

        window.readyGameGlobal.stateFade('in');

        this.add.image(0, 0, 'gameAssets1', 'tight-rope-won-bgd');

        // Add I'm ready button
        const levelTwoBtn = this.add.button(
            this.centreX,
            636,
            'gameAssets1',
            this.startLevelTwo,
            null,
            'im-ready-btn',
            'im-ready-btn'
        );
        levelTwoBtn.anchor.setTo(0.5);
    }

    startLevelTwo() {
        window.readyGameGlobal.stateFade('out', 'TightRopeLevel2');
    }
}

export default GameOneWon;
