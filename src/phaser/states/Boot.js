class Boot extends Phaser.State {
    init() {
        this.stage.backgroundColor = window.readyGameGlobal.stateFadeColor;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        // this.scale.pageAlignHorizontally = true;
        // this.scale.pageAlignVertically = true;
        this.scale.pageAlignVertically = false;
        this.scale.pageAlignHorizontally = true;
        this.stage.disableVisibilityChange = true;

        this.scale.setMinMax(0, 0, 640, 700);
    }

    create() {
        this.state.start('Preload');
    }
}

export default Boot;
