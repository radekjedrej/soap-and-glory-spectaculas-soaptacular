import axios from 'axios';
class WelcomeBack extends Phaser.State {
    create() {
        window.readyGameGlobal.stateFade('in');

        // Flags to prevent buttons and links from being clicked twice as remove event listener is proving
        // to be painful. Also, you don't want people to be able to submit more than once per state load. The event is removed after
        // one click, but just to be safe ;-)
        this.newDetailsCanBeClicked = true;
        this.submitDetailsCanBeClicked = true;

        // I'm using forEach on a node list below which requires this polyfill for IE10
        window.readyGameGlobal.addNodeListForEachPollyfill();

        this.add.image(0, 0, 'gameAssets1', 'mini-game-bgd');

        // // Welcome back html is hidden by default to stop FOC
        this.welcomeBackWrapper = document.querySelector('.welcome-back-wrapper');
        this.welcomeBackWrapper.style.display = 'block';

        // Loading gif is also hidden by default to stop flash of content
        this.loadingGif = document.querySelector('.loader-wrapper');

        // // Add a class to fade the form into view
        this.time.events.add(0, () => {
            this.welcomeBackWrapper.classList.add('fade-in');
        });

        // // Get user name and userId from local storage
        const user = localStorage.getItem('spectaculas_soaptacular_name');
        const userId = localStorage.getItem('spectaculas_soaptacular_id');

        // // add name from local storage to each dynamic name spot
        const dynamicNames = document.querySelectorAll('.dynamic-name');
        dynamicNames.forEach(dn => {
            dn.innerHTML = user;
        });

        // Remove local storage and redirect to data capture page.
        const newDetails = document.querySelector('.new-details');
        newDetails.addEventListener(
            'click',
            event => {
                if (this.newDetailsCanBeClicked) {
                    event.preventDefault();

                    localStorage.removeItem('spectaculas_soaptacular_id');
                    localStorage.removeItem('spectaculas_soaptacular_name');

                    this.welcomeBackWrapper.style.display = 'none';
                    window.readyGameGlobal.stateFade('out', 'DataCapture');
                    this.newDetailsCanBeClicked = false;
                }
            },
            {
                once: true
            }
        );

        const submitButton = document.querySelector('.button-wrapper .enter');
        submitButton.addEventListener(
            'click',
            () => {
                // Show loading gif
                this.loadingGif.style.display = 'flex';

                if (this.submitDetailsCanBeClicked) {
                    axios
                        .post('./api/logic.php', {
                            user_id: userId
                        })
                        .then(({ data }) => {
                            const { winner } = data;

                            this.loadingGif.style.display = 'none';

                            if (winner === true) {
                                window.readyGameGlobal.stateFade('out', 'PrizeWon');
                            } else {
                                window.readyGameGlobal.stateFade('out', 'PrizeLose');
                            }
                        })
                        .catch(error => {
                            console.log('There was an error', error);
                            window.readyGameGlobal.stateFade('out', 'Menu');
                        });

                    this.submitDetailsCanBeClicked = false;
                }
            },
            {
                once: true
            }
        );
    }

    shutdown() {
        // Hide the contact form when this state shuts down
        //this.dataCaptureWrapper.classList.add('hide');
        this.welcomeBackWrapper.style.display = 'none';
    }
}

export default WelcomeBack;
