import axios from 'axios';
import {debounce} from '../../libraries/helpers'

class DataCapture extends Phaser.State {
    toggleGameContent(hide = true) {
        window.readyGameGlobal.wrapper.classList.toggle('wrapper--hide');

        const mobileBg = window.readyGameGlobal.wrapper.querySelector('.js-mobile-bgd-wrapper');

        if(hide === true) {
            mobileBg.style.display = 'none';
        } else {
            mobileBg.style.display = 'block';
        }
    }

    create() {
        window.readyGameGlobal.stateFade('in');

        // I'm using for each on a node list below which requires this polyfill for IE10
        window.readyGameGlobal.addNodeListForEachPollyfill();

        // Flag to prevent people from clicking submit more than once even though the event listener is removed after one click
        this.submitDetailsCanBeClicked = true;

        this.add.image(0, 0, 'gameAssets1', 'mini-game-bgd');


        // Form is hidden by default to stop flash of content
        this.dataCaptureWrapper = document.querySelector('.data-capture-wrapper');
        this.dataCaptureWrapper.style.display = 'block';
        this.toggleGameContent();

        // Loading gif is also hidden by default to stop flash of content
        this.loadingGif = document.querySelector('.loader-wrapper');

        // Age restriction is also hidden by default
        this.ageRestriction = document.querySelector('.age-restriction-overlay');

        this.under13 = this.ageRestriction.querySelector('.under13');
        this.under18 = this.ageRestriction.querySelector('.under18');

        // // Add a class to fade the form into view
        this.time.events.add(0, () => {
            this.dataCaptureWrapper.classList.add('fade-in');
        });

        // Grab our errors div
        this.errorDiv = this.dataCaptureWrapper.querySelector('.errors');

        // FORM SCRIPTS
        const form = document.querySelector('.js-data-capture-form');

        // // Add invalid classes to invlid fields for styling as opposed to using :invalid pseudo selector
        // // as all fields are invalid on page load and therefore would show up with an initial red border.
        const invalidClassName = 'invalid';
        const inputs = document.querySelectorAll('input, select');

        // The function the be run to remove invalid class.
        // This will be debounced so that the function can't be fired
        // for 0.5s. This function needs to be a normal function
        // because we want the scope of `this` to be the function itself.
        const onInputValid = function() {
            if (this.validity.valid) {
                this.classList.remove(invalidClassName);
            }
        }

        inputs.forEach(input => {
            // Add a class on submit when the input is invalid
            input.addEventListener('invalid', () => input.classList.add(invalidClassName));

            // Remove the class when the input becomes valid
            // 'input' will fire each time the user types
            input.addEventListener('input', debounce(onInputValid, 500));
        });

        // Set up our form event handler
        form.addEventListener(
            'submit',
            event => {
                if (this.submitDetailsCanBeClicked) {
                    event.preventDefault();

                    // Show loading gif
                    this.loadingGif.style.display = 'flex';

                    // Get all the form inputs
                    const name = form.elements.name.value;
                    const surname = form.elements.surname.value;
                    const email = form.elements.email.value;
                    const day = form.elements.day.value;
                    const month = form.elements.month.value;
                    const year = form.elements.year.value;
                    const address1 = form.elements.address1.value;
                    const city = form.elements.city.value;
                    const postcode = form.elements.postcode.value;
                    const country = form.elements.country.value;
                    const tandc = form.elements.tandc.checked;
                    const signup = form.elements.signup.checked;
                    const signup_circus = form.elements.signup_circus.checked;

                    // console.log({
                    //     name,
                    //     surname,
                    //     email,
                    //     day,
                    //     month,
                    //     year,
                    //     address1,
                    //     city,
                    //     postcode,
                    //     country,
                    //     tandc,
                    //     signup
                    // });

                    // console.log(
                    //     'age check:',
                    //     this.ageCheck(year, this.convertMonthToInteger(month), day, country)
                    // );

                    if (
                        this.ageCheck(year, this.convertMonthToInteger(month), day, country) !==
                        true
                    ) {
                        return;
                    }

                    axios
                        .post('./api/capture.php', {
                            name,
                            surname,
                            email,
                            day,
                            month,
                            year,
                            address1,
                            city,
                            postcode,
                            country,
                            tandc,
                            signup,
                            signup_circus
                        })
                        .then(({ data }) => {
                            const { valid_fields, last_id: userId } = data;

                            if (valid_fields === true) {
                                // User was submitted or updated

                                // Add local storage here. If value already exists what happens?
                                localStorage.setItem('spectaculas_soaptacular_id', userId);
                                localStorage.setItem('spectaculas_soaptacular_name', name);

                                // Clear the form
                                form.reset();

                                this.dataCaptureWrapper.style.display = 'none';
                                // If we are successful in adding our user to the database, we need to run our game logic
                                // This will return a true if win, false if lose.
                                // This should be a get request but for some reason it was just returning the whole of the php file.
                                // I spent toooooo long trying to adjust headers and content type but couldn't get it to work.
                                // If you can, let me know how you did it!
                                axios
                                    .post('./api/logic.php', {
                                        user_id: userId
                                    })
                                    .then(({ data }) => {
                                        const { winner } = data;

                                        this.loadingGif.style.display = 'none';

                                        if (winner === true) {
                                            window.readyGameGlobal.stateFade('out', 'PrizeWon');
                                        } else {
                                            window.readyGameGlobal.stateFade('out', 'PrizeLose');
                                        }
                                    })
                                    .catch(error => {
                                        console.log('There was an error', error);

                                        this.loadingGif.style.display = 'none';

                                        window.readyGameGlobal.stateFade('out', 'Menu');
                                    });

                                // Reset the errors div just incase it was made visible on a previous visit.
                                this.errorDiv.style.display = 'none';
                            } else {
                                // Our response states we have some blank fields
                                // Display the error div as it's hidden by default
                                this.errorDiv.style.display = 'block';

                                // remove any previous errors.
                                this.errorDiv.innerHTML = '';

                                // Loop through our returned error object and output message
                                for (const prop in data.errors) {
                                    const value = data.errors[prop];

                                    const returnValue = `<p>${value}</p>`;
                                    this.errorDiv.insertAdjacentHTML('beforeend', returnValue);
                                }

                                // Scroll to top to see errors
                                document.querySelector('.page-overlay').scrollTop = 0;
                            }
                        })
                        .catch(error => {
                            console.log('There was an error', error);

                            this.loadingGif.style.display = 'none';

                            window.readyGameGlobal.stateFade('out', 'Menu');
                        });
                }
            },
            // Setting once to true will automatically remove the event listener after it is triggerd once. This prevents
            // it from being added mutltiple times each time the state is loaded.
            { once: true }
        );
    }

    ageCheck(year, month, day, country) {
        // We need to check if the user is old enough to play
        const personsDob = new Date(year, month, day);
        const currentDate = new Date();

        let age = currentDate.getFullYear() - personsDob.getFullYear();

        if (
            currentDate.getMonth() < month ||
            (currentDate.getMonth() == month && currentDate.getDate() < day)
        ) {
            age--;
        }

        //console.log({ personsDob, currentDate, age });

        if (country === 'Ireland' && age < 18) {
            this.loadingGif.style.display = 'none';
            this.ageRestriction.style.display = 'flex';
            this.under18.style.display = 'block';
            return;
        }

        if (age < 13) {
            this.loadingGif.style.display = 'none';
            this.ageRestriction.style.display = 'flex';
            this.under13.style.display = 'block';
            return;
        }
        return true;
    }

    convertMonthToInteger(month) {
        switch (month) {
            case 'January':
                return 0;
                break;
            case 'February':
                return 1;
                break;
            case 'March':
                return 2;
                break;
            case 'April':
                return 3;
                break;
            case 'May':
                return 4;
                break;
            case 'June':
                return 5;
                break;
            case 'July':
                return 6;
                break;
            case 'August':
                return 7;
                break;
            case 'September':
                return 8;
                break;
            case 'October':
                return 9;
                break;
            case 'November':
                return 10;
                break;
            case 'December':
                return 11;
                break;
            default:
                return 0;
        }
    }
    shutdown() {
        // Hide the contact form when this state shuts down
        //this.dataCaptureWrapper.classList.add('hide');
        this.dataCaptureWrapper.style.display = 'none';

        this.toggleGameContent(false);
    }
}

export default DataCapture;
