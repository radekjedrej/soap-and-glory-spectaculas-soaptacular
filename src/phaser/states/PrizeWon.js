class PrizeWon extends Phaser.State {
    create() {
        // State Helpers
        this.centreX = this.game.width / 2;
        this.centreY = this.game.height / 2;

        window.readyGameGlobal.stateFade('in');

        // Add background image
        this.add.image(0, 0, 'gameAssets2', 'prize-won-bgd');

        const playAgain = this.add.button(
            this.centreX,
            this.centreY + 300,
            'gameAssets1',
            this.playAgain,
            null,
            'want-to-play-again-btn',
            'want-to-play-again-btn'
        );
        playAgain.anchor.setTo(0.5);
    }

    playAgain() {
        
        gtag('event', 'page', {
            'event_category': 'Game State',
            'event_label': 'Game Started'
        });
        //Send our total plays event to google analytics
        //window.readyGameGlobal.sendClickEvent('Total number of plays', 'played', 1);
        window.readyGameGlobal.stateFade('out', 'TightRope');
    }
}

export default PrizeWon;
