class Menu extends Phaser.State {
    create() {
        // State Helpers
        this.centreX = this.game.width / 2;
        this.centreY = this.game.height / 2;

        window.readyGameGlobal.stateFade('in');

        this.add.image(0, 0, 'gameAssets1', 'menu-bgd');

        // Add our buttons
        const playNowBtn = this.add.button(
            40,
            210,
            'gameAssets1',
            this.playNow,
            null,
            'play-now-btn',
            'play-now-btn'
        );

        const howToPlayBtn = this.add.button(
            40,
            275,
            'gameAssets1',
            this.howToPlay,
            null,
            'how-to-play-btn',
            'how-to-play-btn'
        );

        const prizesBtn = this.add.button(
            40,
            340,
            'gameAssets1',
            this.prizes,
            null,
            'prizes-btn',
            'prizes-btn'
        );

        // Show our mobile background
        const bgd = document.querySelector('.mobile-bgd-wrapper');
        bgd.style.display = 'block';
    }

    playNow() {

        gtag('event', 'page', {
            'event_category': 'Game State',
            'event_label': 'Game Started'
        });

        //Send our total plays event to google analytics
        //window.readyGameGlobal.sendClickEvent('Total number of plays', 'played', 1);
        // window.readyGameGlobal.stateFade('out', 'WelcomeBack');
        window.readyGameGlobal.stateFade('out', 'TightRope');
        console.log('play now');
    }

    howToPlay() {
        window.readyGameGlobal.stateFade('out', 'HowToPlay');
    }

    prizes() {
        window.readyGameGlobal.stateFade('out', 'Prizes');
    }
}

export default Menu;
