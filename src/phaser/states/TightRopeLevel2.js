import Ball from '../objects/Ball';

class TightRopeLevel2 extends Phaser.State {
    create() {
        // State Helpers
        this.centreX = this.game.width / 2;
        this.centreY = this.game.height / 2;

        window.readyGameGlobal.stateFade('in');

        const settings = window.readyGameSettings.level2;
        // constants
        // Although used in both games, if we put these under readyGameGlobal they will be exposed and it
        // would be possible to change them.
        this._GAME_TIME = settings.gameTime || 5;
        this._ANGLE_THAT_PLAYER_WILL_FALL = settings.angleThatPlayerWillFall || 50; // The player will fall if they get to this angle
        this._ANGLE_THAT_PLAYER_CAN_CLICK = settings.angleThatPlayerCanClick || 5; // The player must be past this angle +- before they can click left or right.
        this._SWAY_SPEED_MIN = settings.swaySpeedMin || 2000; // We randomly generate the sway speed on left or right click. Set the min and max here. Max must be greater than min
        this._SWAY_SPEED_MAX = settings.swaySpeedMax || 3000;
        this._SWAY_TWEEN_EASING_TYPE = settings.easing || 'Cubic.easeIn'; // Refer to Phaser for available easing
        this._BALL_FREQUENCY = settings.ballFrequency || 4000;

        // Flags and variables
        this.canClickLeft = false;
        this.canClickRight = false;
        this.randomSwaySpeed = 2000;

        this.add.image(0, 0, 'gameAssets1', 'mini-game-bgd');


        this.addGameTimer(); // Add the game timer
        this.addCountDownClock(); // Add our countdown clock
        this.addTightRope(); // Add tight rope
        this.addTightRopeWoman(); // Add player
        this.addPlayerControls(); // Add our player controls for clicking left and right
        this.addBallTimer(); // Start throwing our balls
        this.setupPlayerSwayTweens(); // Setup player sway tweens
        this.setupStartingPlayerWobbleTween(); // Setup the initial player wobble
        this.wobbleTightRope(); // Start wobbling the tight rope


        this.addTapToHitAway(); // Add info that you have to protect RopeWomen from being hitted with balls




        this.ballsGroup = this.add.group(); // Add a group for our balls. Added here so it stays on top

        // Create a custom signal to so the ball can emit when it has finished it's tween
        this.ballSignal = new Phaser.Signal();

        // Set up our listener
        this.ballSignal.addOnce(() => {
            // Listen for any signals coming from our balls
            this.stopTimers();
            this.playerFallTween();
        });
    }

    addTapToHitAway() {
        // Add tight rope
        this.tapToProtect = this.add.image(
            this.centreX,
            this.centreY + 200,
            'gameAssets1',
            'tap-to-hit-away'
        );
        this.tapToProtect.anchor.setTo(0.5, 0.5);

        this.instExpire = this.time.now + 3000;

        setTimeout(() => {
            this.tapToProtect.destroy();
        }, 1500)

    }

    addGameTimer() {
        this.remainingTime = this._GAME_TIME;
        this.gameTimer = this.time.create();
        this.gameTimer.repeat(Phaser.Timer.SECOND, this._GAME_TIME, () => {
            this.remainingTime = this.remainingTime - 1;
        });
        this.gameTimer.start(); // Starts the game timer

        // Timer has finished which means game has been won
        this.gameTimer.onComplete.add(() => {
            this.gameWon();
        });
    }

    addCountDownClock() {
        const clockBgd = this.add.image(60, 60, 'gameAssets1', 'clock-bgd');
        clockBgd.anchor.setTo(0.5);

        const clockHand = this.add.image(clockBgd.x, clockBgd.y + 12, 'gameAssets1', 'clock-hand');
        clockHand.anchor.setTo(0.5, 0.9);

        this.clockHandTween = this.add.tween(clockHand).to(
            {
                angle: 180,
                y: clockHand.y - 7 // slight adjustment to the wonky clock hand
            },
            Phaser.Timer.SECOND * this._GAME_TIME,
            'Linear',
            true
        );
    }

    addTightRope() {
        // Add tight rope
        this.tightRope = this.add.image(
            this.centreX,
            this.game.height,
            'gameAssets1',
            'tight-rope'
        );
        this.tightRope.anchor.setTo(0.5, 1);
    }

    addTightRopeWoman() {
        // Add our player
        this.tightRopeWoman = this.add.sprite(
            this.centreX,
            this.game.height - 200,
            'gameAssets1',
            'tight-rope-woman'
        );
        this.tightRopeWoman.anchor.setTo(0.5, 1);
    }

    addPlayerControls() {
        // Player controls
        const moveLeftBtn = this.add.button(
            this.centreX + 150,
            this.centreY + 200,
            'gameAssets1',
            () => {
                if (
                    this.tightRopeWoman.angle > this._ANGLE_THAT_PLAYER_CAN_CLICK &&
                    this.canClickLeft
                ) {
                    this.generateSwaySpeed();
                    this.swayLeft.pendingDelete = false;
                    this.swayRight.stop();
                    this.swayLeft.updateTweenData('duration', this.randomSwaySpeed);
                    this.swayLeft.start();
                    this.canClickRight = true;
                    this.canClickLeft = false;
                }
            },
            null,
            'left-btn',
            'left-btn'
        );
        moveLeftBtn.anchor.setTo(0.5);

        const moveRightBtn = this.add.button(
            this.centreX - 150,
            this.centreY + 200,
            'gameAssets1',
            () => {
                if (
                    this.tightRopeWoman.angle < -this._ANGLE_THAT_PLAYER_CAN_CLICK &&
                    this.canClickRight
                ) {
                    this.generateSwaySpeed();
                    this.swayRight.pendingDelete = false;
                    this.swayLeft.stop();
                    this.swayRight.updateTweenData('duration', this.randomSwaySpeed);
                    this.swayRight.start();
                    this.canClickLeft = true;
                    this.canClickRight = false;
                }
            },
            null,
            'right-btn',
            'right-btn'
        );
        moveRightBtn.anchor.setTo(0.5);
    }

    addBallTimer() {
        this.ballreleaseTimer = this.time.create();
        this.ballreleaseTimer.loop(this._BALL_FREQUENCY, () => {
            const ball = new Ball(this.game, this.ballSignal);

            this.ballsGroup.add(ball);
        });

        this.ballreleaseTimer.start();
    }

    setupStartingPlayerWobbleTween() {
        this.playerWobbleTween = this.add.tween(this.tightRopeWoman).to(
            {
                angle: -5
            },
            500,
            'Linear',
            true,
            0,
            this.rnd.integerInRange(0, 2),
            true
        );

        // Random fall to the left or right to start the game
        this.playerWobbleTween.onComplete.add(() => {
            this.canClickLeft = true;
            this.canClickRight = true;
            this.rnd.integerInRange(0, 1) === 0 ? this.swayLeft.start() : this.swayLeft.start();
        });
    }

    setupPlayerSwayTweens() {
        this.swayLeft = this.add.tween(this.tightRopeWoman).to(
            {
                angle: -this._ANGLE_THAT_PLAYER_WILL_FALL
            },
            this.randomSwaySpeed,
            this._SWAY_TWEEN_EASING_TYPE,
            false
        );

        this.swayLeft.onComplete.add(() => {
            this.stopTimers();
            this.playerFallTween();
        });

        this.swayRight = this.add.tween(this.tightRopeWoman).to(
            {
                angle: this._ANGLE_THAT_PLAYER_WILL_FALL
            },
            this.randomSwaySpeed,
            this._SWAY_TWEEN_EASING_TYPE,
            false
        );

        this.swayRight.onComplete.add(() => {
            this.stopTimers();
            this.playerFallTween();
        });
    }

    playerFallTween() {
        this.add
            .tween(this.tightRopeWoman)
            .to(
                {
                    y: this.game.height + 200
                },
                800,
                'Linear',
                true
            )
            .onComplete.add(() => this.gameOver());

        this.add.tween(this.tightRopeWoman.scale).to(
            {
                y: -1
            },
            800,
            'Linear',
            true
        );

        // Stop the wobbling tight rope
        this.wobbleTightRopeTween.stop();
    }

    wobbleTightRope() {
        this.wobbleTightRopeTween = this.add.tween(this.tightRope).to(
            {
                x: this.tightRope.x - 1
            },
            100,
            'Linear',
            true,
            0,
            -1,
            true
        );
    }

    stopTimers() {
        this.gameTimer.stop();
        this.clockHandTween.stop();
        this.ballreleaseTimer.destroy(); // Remove our ball timer
    }

    gameOver() {
        window.readyGameGlobal.stateFade('out', 'Lose');
    }

    gameWon() {
        // Tween the player back upright
        this.canClickLeft = false;
        this.canClickRight = false;
        this.swayLeft.stop();
        this.swayLeft.stop();

        this.add
            .tween(this.tightRopeWoman)
            .to(
                {
                    angle: 0
                },
                500,
                'Linear',
                true
            )
            .onComplete.add(() => {

                if (
                    localStorage.getItem('spectaculas_soaptacular_name') &&
                    localStorage.getItem('spectaculas_soaptacular_id') &&
                    localStorage.getItem('spectaculas_soaptacular_id') > 0
                ) {
                    window.readyGameGlobal.stateFade('out', 'WelcomeBack')
                } else {
                    window.readyGameGlobal.stateFade('out', 'DataCapture')
                }

            });
    }

    generateSwaySpeed() {
        this.randomSwaySpeed = this.rnd.integerInRange(this._SWAY_SPEED_MIN, this._SWAY_SPEED_MAX);
    }

    render() {
        //this.debugText();
    }

    debugText() {
        this.game.debug.text(`Game Timer: ${this.randomSwaySpeed}`, 20, 20);
        this.game.debug.text(`Woman Angle: ${this.tightRopeWoman.angle}`, 20, 40);
        this.game.debug.text(`Sway Speed: ${this.randomSwaySpeed}`, 20, 60);
    }
}

export default TightRopeLevel2;
