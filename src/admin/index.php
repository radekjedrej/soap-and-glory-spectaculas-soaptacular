<?php
    
    require __DIR__ . '/../api/init.php';
    
    // output headers so that the file is downloaded rather than displayed
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=spectaculas_soaptacular.csv');
    
    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');

     // output the column headings
    fputcsv($output, array('User ID', 'Name', 'Surname', 'email', 'DOB', 'Address 1', 'City', 'Postcode', 'Country', 'T & C', 'Signup', 'Total Prizes Won', 'Total Played'));

    $results = $dbh->query("SELECT user_id, name, surname, email, dob, address1, city, postcode, country, tandc, signup, total_prizes_won, total_played FROM users ORDER BY user_id")->fetchALL(PDO::FETCH_ASSOC);
    
    foreach($results as $row) {
        
        fputcsv($output, $row);
    }

    fclose($output);
