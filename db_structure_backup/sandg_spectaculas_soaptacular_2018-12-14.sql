# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.23)
# Database: sandg_spectaculas_soaptacular
# Generation Time: 2018-12-14 13:03:04 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table prizes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `prizes`;

CREATE TABLE `prizes` (
  `prize_id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `prize` varchar(255) NOT NULL DEFAULT '',
  `total_remaining` int(3) NOT NULL,
  PRIMARY KEY (`prize_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `prizes` WRITE;
/*!40000 ALTER TABLE `prizes` DISABLE KEYS */;

INSERT INTO `prizes` (`prize_id`, `prize`, `total_remaining`)
VALUES
	(1,'instant prize',300);

/*!40000 ALTER TABLE `prizes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `surname` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `address1` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) DEFAULT '',
  `postcode` varchar(50) NOT NULL DEFAULT '',
  `country` varchar(255) NOT NULL DEFAULT '',
  `tandc` int(11) DEFAULT NULL,
  `signup` tinyint(1) DEFAULT '0',
  `total_prizes_won` tinyint(1) NOT NULL,
  `total_played` int(11) NOT NULL DEFAULT '1',
  `security` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table winners
# ------------------------------------------------------------

DROP TABLE IF EXISTS `winners`;

CREATE TABLE `winners` (
  `winner_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `winner_email` varchar(255) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`winner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
