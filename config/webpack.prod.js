const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = env => {
    return {
        entry: {
            main: ['@babel/polyfill', './src/main.js']
        },
        mode: 'production',
        output: {
            filename: '[name]-bundle.js',
            path: path.resolve(__dirname, '../dist'),
            publicPath: './'
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    use: [
                        {
                            loader: 'babel-loader'
                        }
                    ],
                    exclude: /node_modules/
                },
                {
                    test: /\.scss$/,
                    use: [
                        { loader: MiniCSSExtractPlugin.loader },
                        {
                            loader: 'css-loader',
                            options: {
                                minimize: false // use OptimizeCssAssetsPlugin instead
                            }
                        },
                        { loader: 'postcss-loader' },
                        { loader: 'sass-loader' }
                    ]
                },
                {
                    test: /\.(jpg|gif|png|svg)$/,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                name: 'build-assets/[name].ext'
                            }
                        }
                    ]
                },
                {
                    test: /\.html$/,
                    use: [
                        {
                            loader: 'html-loader',
                            options: {
                                attrs: ['img:src']
                            }
                        }
                    ]
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf|svg)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: 'fonts/[name].[ext]'
                            }
                        }
                    ]
                }
            ]
        },
        plugins: [
            //new webpack.HotModuleReplacementPlugin(),
            new CleanWebpackPlugin(['dist'], {
                root: path.join(__dirname, '..')
            }),
            new OptimizeCssAssetsPlugin(),
            new MiniCSSExtractPlugin({
                filename: '[name]-[contenthash].css'
            }),
            new HtmlWebpackPlugin({
                template: './src/index.html',
                meta: { viewport: 'width=device-width, initial-scale=1' }
            }),
            new CopyWebpackPlugin([
                { from: './src/libraries', to: 'libraries' },
                { from: './src/build-assets', to: 'build-assets' },
                { from: './src/api', to: 'api' },
                { from: './src/admin', to: 'admin' },
                { from: './src/gameSettingsTemp.js', to: './' }
            ]),
            new UglifyJSPlugin()
        ]
    };
};
