const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        main: ['@babel/polyfill', './src/main.js']
    },
    mode: 'development',
    output: {
        filename: '[name]-bundle.js',
        path: path.resolve(__dirname, '../dist'),
        publicPath: '/'
    },
    devServer: {
        // https://stackoverflow.com/questions/35412137/how-to-get-access-to-webpack-dev-server-from-devices-in-local-network
        // To get your computers IP on the network go to `system-prefereces/network-utility`
        // and under wifi you will see your IP address for the network.
        contentBase: 'dist',
        host: '0.0.0.0', // This lets the server listen for requests from the network, not just localhost
        port: 8080, // Port the site will be served using
        // `enter`
        // historyApiFallback: true,
        overlay: true,
        // hot: true
        proxy: {
            '/': {
                target: {
                    host: 'sandg.circus',
                    protocol: 'http:',
                    port: 80
                },
                changeOrigin: true,
                secure: false
            }
        }
    },
    devtool: 'source-maps',
    module: {
        rules: [
            {
                test: /\.js$/,
                use: [
                    {
                        loader: 'babel-loader'
                    }
                ],
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' },
                    { loader: 'postcss-loader' },
                    { loader: 'sass-loader' }
                ]
            },
            {
                test: /\.(jpg|gif|png|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            name: 'build-assets/[name].ext'
                        }
                    }
                ]
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {
                            attrs: ['img:src']
                        }
                    }
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'fonts/[name].[ext]'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new CopyWebpackPlugin([
            { from: './src/libraries', to: 'libraries' },
            { from: './src/build-assets', to: 'build-assets' },
            { from: './src/api', to: 'api' },
            { from: './src/gameSettingsTemp.js', to: './' }
        ])
    ]
};
